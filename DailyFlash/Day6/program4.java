/* Que 4: WAP to print strong numbers in a given range.
Input: 1 to 10
*/
import java.io.*;
class StrongNumber{
        public static void main(String[]args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter number of rows");
                int start=Integer.parseInt(br.readLine());
                System.out.println("Enter number of coloumns");
                int end = Integer.parseInt(br.readLine());
		for(int i=start;i<=end;i++){
			   int num2=i;
			   int num1=i;
			   int sum=0;
			   int fact=1;
      			   while(num1!=0){   
          			   int rem=num1%10;
          			   num1=num1/10;
          			   for(int j=1;j<=rem;j++){
					   fact=fact*j;
          				   sum=sum+fact;
				   }
      			   if(sum==num2)
				   System.out.println(i+" ");
			   }
		   }
	}
}
