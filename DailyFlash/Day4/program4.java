/*Que 4: WAP to print the sum of digits in a given range.
Input: 1 to 10
Input: 21 to 30
*/
import java.io.*;
class SumofDigits{
        public static void main(String[]arg)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter stating number");
                int start = Integer.parseInt(br.readLine());
                System.out.println("Enter ending numbers");
                int end = Integer.parseInt(br.readLine());
		int sum=0;
		for(int i=start;i<=end;i++){
			sum=sum+i;
		}
		System.out.println("sum of digits between range "+start+" and "+end+" is: "+sum);
	}
}

