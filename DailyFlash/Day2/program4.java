/*Que 4 : WAP to print the composite numbers in the given range
Input: start:1
end:100
*/
import java.io.*;
class CompositeNum{
	public static void main(String[] args)throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter starting number of range");
	        int start=Integer.parseInt(br.readLine());
        	System.out.println("Enter ending number of range");
	        int end=Integer.parseInt(br.readLine());
		System.out.print("composite numbers between "+start+"-"+end+" are :");
	        for(int i=start;i<=end;i++){        	    
			int count=0;
            		for(int j=1;j<=i;j++){
                		if(i%j==0){
                    		count++;
                		}
            		}
            		if(count>2)
                	System.out.print(i+" ");
		}
		System.out.println();
    	}
}

