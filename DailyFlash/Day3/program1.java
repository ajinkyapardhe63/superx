/*Oue 1 : WAP to print the following pattern 
  Take input from user
  A B C D
  D C B A
  A B C D
  D C B A
*/
import java.io.*;
class Pattern1{
	public static void main(String[]arg)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of row");
		int row = Integer.parseInt(br.readLine());
		System.out.println("Enter number of coloumns");
		int col = Integer.parseInt(br.readLine());
		int ch=65;
		for(int i=0,j=0;i<row&&j<col;j++){
			if(j==row-1){
				if(i%2==0){
					System.out.println((char)ch);
					i++;
					j=-1;
					ch=68;
				}
				else{
					System.out.println((char)ch);
                                        i++;
                                        j=-1;
                                        ch=65;
				}
			}
			else{
				if(i%2==0){
					System.out.print((char)ch+" ");
					ch++;
				}
				else{
					System.out.print((char)ch+" ");
                                        ch--;
				}
			}
		}
	}
}

