//Que 5 : WAP to check whether the string contains vowels and return the count of vowels.
import java.io.*;
class ReturnNoOfVowels{
	int Vowels(String str){
		int i=0;
		int vowelsNum=0;
		while(i<str.length()){
			if(str.charAt(i)=='a'||str.charAt(i)=='e'||str.charAt(i)=='i'||str.charAt(i)=='o'||str.charAt(i)=='u')
				vowelsNum++;
			i++;
		}
		return vowelsNum;
	}
}
class VowelsMain{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a String");
		String str=br.readLine();
		br.close();
		ReturnNoOfVowels obj=new ReturnNoOfVowels();
		int NoVowels=obj.Vowels(str);
		System.out.println(str+" contains "+NoVowels+" Number of Vowels");
	}
}
