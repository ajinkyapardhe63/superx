/*Print this pattern
  4
  5  7
  6  9  12
  7  11 15 19
*/
import java.io.*;
class Pattern3{
        public static void main(String[]args)throws IOException{
                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Number of rows");
                int row=Integer.parseInt(br.readLine());
                System.out.println("Enter Number of columns");
                int col=Integer.parseInt(br.readLine());
                int num=4;
                for(int i=0,j=0;i<row&&j<col;j++){
                        if(j==i){
                                if(i%2==0){
                                        System.out.println(num);
                                        num=num-(i*j)+(i+1)-i*2;
                                }
                                else{
                                        System.out.println(num);
                                        num=num-(i*j)-(i-1);
                                }
                                j=-1;
                                i++;
                        }
                        else{
                                System.out.print(num+" ");
                                num=num+(i+1);
                        }
                }
        }
}

