/*print this pattern
  1  2  3  4
  4  5  6  7
  6  7  8  9
  7  8  9  10
*/
import java.io.*;
class Pattern1{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows");
		int row=Integer.parseInt(br.readLine());
		System.out.println("Enter number of columns");
		int col=Integer.parseInt(br.readLine());
		int num=1;
		for(int i=0,j=0;i<row&&j<col;j++){
			if(j==col-1){
				System.out.println(num+" ");
				j=-1;
				i++;
				num=num-i+1;
			}
			else{
				System.out.print(num+" ");
				num++;
			}
		}
	}
}
