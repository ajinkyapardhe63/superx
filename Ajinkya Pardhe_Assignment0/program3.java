/*print this pattern 
  c d e f
  D C B A
  e f g h
  F E D C
  g h i j

*/
class Pattern{
	public static void main(String[]args){
		char ch = 'f';
		int typecast;

		for(int i=0;i<4;i++){
			for(int j=0;j<4;j++){
				if(i%2==0){
					typecast=Character.toUpperCase((int)ch-2);
					System.out.print((char)typecast+" ");
					ch--;

				}
				else{
					typecast=Character.toLowerCase((int)ch+3);
                                        System.out.print((char)typecast+" ");
                             		ch++;
				}
			}
			if(i%2==1){
				typecast=(int)ch+2;
				ch=(char)typecast;
			}
			
			System.out.println();
		}
	}
}


              
