import java.io.*;
class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		int arr2[]=new int[size];
		System.out.println("Enter Elements in array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0)
				arr2[i]=1;
			
			else
				arr2[i]=0;
		}

		System.out.print("output - [");
		for(int i=0;i<arr2.length-1;i++){
			System.out.print(arr2[i]+",");
		}
		System.out.println(arr2[arr2.length-1]+"]");


	}
}
