//Que 3: WAP to check whether the given number is Duck number or not.
import java.io.*;
class FindDuckNum{
	boolean isduck(String num){
		boolean flag=false;
		int i=0;
		while(i<num.length()){
			if(num.charAt(i)=='0'&& i<1)
				break;
			else if(num.charAt(i)=='0'){
				flag=true;
				break;
			}
			i++;
		}
		return flag;
	}
}
class DuckNum{
	public static void main(String[]args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a Number");
		String num=br.readLine();
		FindDuckNum obj=new FindDuckNum(); 
		br.close();
		if(obj.isduck(num))
			System.out.println(num+" is a duck number");
		else
			System.out.println(num+" is not a duck number");
	}
}
