// Q1) Write a program to print the factorial of Even digits in a number //

import java.io.*;
class evenFact{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a Number");
		int num=Integer.parseInt(br.readLine());
		int rem=0;
		while(num!=0){
			rem=num%10;
			num=num/10;
			if(rem%2==0){
				int fact=1;
				for(int i=1;i<=rem;i++){
					fact=fact*i;
				}
				System.out.print(fact+" ");
			}
		}
		System.out.println();
	}
}
