//Que 3: WAP to check whether the given no is odd or even//
import java.io.*;
class EvenOdd{
        public static void main(String[]args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());
		if(num%2==0)
			System.out.println(num+" is an even number");
		else
			System.out.println(num+" is an odd number");
	}
}

