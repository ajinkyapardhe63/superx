//Que 5: WAP to count the size of given string (without using inbuilt method)
import java.io.*;
class StringSize{
        public static void main(String[]args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter a String");
                String str=br.readLine();
		str=str+"\0";
		br.close();
		int i=0;
		while(str.charAt(i)!='\0'){
			i++;
		}
		System.out.println(i);
	}
}
