/*Que 1: WAP to print the following pattern
Take input from the user
A B C D
# # # #
A B C D
# # # #
A B C D
*/
import java.io.*;
class Pattern1{
        public static void main(String[]arg)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter number of row");
                int row = Integer.parseInt(br.readLine());
                System.out.println("Enter number of coloumns");
                int col = Integer.parseInt(br.readLine());
		char ch='A';
		for(int i=0,j=0;i<row&&j<col;j++){
			if(i%2==0){
				if(j==col-1){
					System.out.println(ch);
					ch='A';
					i++;
					j=-1;
				}
				else{
					System.out.print(ch+" ");
					ch++;
				}
			}
			else{
				if(j==col-1){
					System.out.println("#");
					i++;
					j=-1;
				}
				else{
					System.out.print("# ");
				}
			}
		}
	}
}

