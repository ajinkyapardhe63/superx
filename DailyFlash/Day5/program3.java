//Que 3: WAP to check whether the given number is a strong number or not.
import java.io.*;
class StrongNumber{
	boolean Strong(int num){
		int store=num;
		int sum=0;
		while(num!=0){
			int rem=num%10;
			num=num/10;
			int product=1;
			for(int i=1;i<=rem;i++){
				product=product*i;
			}
			sum=sum+product;
		}
		if(sum==store)
			return true;
		else
			return false;
	}
}
class StrongNumberMain{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a Number to check if it is Strong Number or not....");
		int num=Integer.parseInt(br.readLine());
		StrongNumber obj=new StrongNumber();
		boolean strng=obj.Strong(num);
		if(strng == true)
			System.out.println(num+" is a strong number");
		else
			System.out.println(num+" is not a strong number");
	}
}



