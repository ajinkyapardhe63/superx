/*Que 2: WAP to print the following pattern
Take row input from the user
a
A B
a b c
A B C D
*/
import java.io.*;
class Pattern1{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows");
                int row=Integer.parseInt(br.readLine());
                System.out.println("Enter number of coloumns");
                int col = Integer.parseInt(br.readLine());
		char ch='a';

		for(int i=0,j=0;i<row&&j<col;j++){
			if(i%2==0){
				if(j==i){
					System.out.println(ch);
					j=-1;
					i++;
					ch='A';
				}
				else{
					System.out.print(ch+" ");
					ch++;
				}
			}
			else{
				if(j==i){
					System.out.println(ch);
					j=-1;
					i++;
					ch='a';
				}
				
				else{
					System.out.print(ch+" ");
					ch++;
				}
				
			}
		}
	}
}





