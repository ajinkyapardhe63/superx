/*Que 4 : WAP to print each reverse numbers in the given range
Input: start:25435
end: 25449
*/
import java.io.*;
class ReverseNum{
        public static void main(String[]arg)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter starting number");
                int start = Integer.parseInt(br.readLine());
                System.out.println("Enter ending number");
                int end  = Integer.parseInt(br.readLine());
                int num;
                for(int i=start;i<=end;i++){
			num=i;
			int rev=0,rem;
			while(num!=0){
				rem=num%10;
				rev=rev*10+rem;
				num=num/10;
			}
			System.out.println(rev);
		}
	}
}

