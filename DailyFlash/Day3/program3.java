//Que 3 : WAP to check whether the given no is a palindrome number or not.
import java.io.*;
class PalindromeNum{
        public static void main(String[]arg)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter a number");
                int num = Integer.parseInt(br.readLine());
		int store=num;
		int rem,rev=0;
		while(num!=0){
			rem=num%10;
			rev=rev*10+rem;
			num=num/10;
		}
		if(rev==store)
			System.out.println(store+" is palindrome number");
		else
			System.out.println(store+" is not a palindrome number");
	}
}


