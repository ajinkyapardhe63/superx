import java.io.*;
class Successivesum{
	public static void main(String []args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a number");
		int num=Integer.parseInt(br.readLine());
		int rev=0,rem=0,count=0;
		while(num!=0){
			rem=num%10;
			rev=rev*10+rem;
			num=num/10;
			count++;
		}
		System.out.println("reverse of this number is "+rev);
		int sumarr[]=new int[count];
		int sum=0;
		rem=0;
		count=0;
		int store=0;
		while(rev!=0){
			rem=rev%10;
			sum=store+rem;
			store=rem;
			sumarr[count]=sum;
			rev=rev/10;
			count++;
		}
		System.out.print("Successive sum of the reverse number is - [" );
		for (int i =0;i<sumarr.length-1;i++){
			System.out.print(sumarr[i]+",");
		}
		System.out.println(sumarr[sumarr.length-1]+"]");

	}
}

