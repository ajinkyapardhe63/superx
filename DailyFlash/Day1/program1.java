/*Que 1 : WAP to print the following pattern
Take input from user
1 2 3 4
2 3 4 5
3 4 5 6
4 5 6 7
*/
import java.io.*;
class Pattern1{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no. of rows");
		int row=Integer.parseInt(br.readLine());
		System.out.println("Enter no. of coloumns");
                int col=Integer.parseInt(br.readLine());
		int num=1;
		for(int i=0,j=0;i<row&&j<col;j++){
			if(j==row-1){
				System.out.println(num);
				num=0;
				num=num+i+2;
				i++;
				j=-1;
			}
			else{
				System.out.print(num+" ");
				num++;
			}
		}
	}
}


