/*Que 4 : WAP to print the odd numbers in the given range
Input: start:1
end:10
*/
import java.io.*;
class Oddnums{
        public static void main(String[]args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter starting number");
                int start=Integer.parseInt(br.readLine());
                System.out.println("Enter ending number");
                int end=Integer.parseInt(br.readLine());
		System.out.print("Odd numbers between "+start+"-"+end+" are[");
		for(int i=start;i<=end;i++){
			if(i%2==1)
				System.out.print(i+" ");
		}
		System.out.println("]");
	}
}

