/*Que 2: WAP to print the following pattern
Take row input from the user
A
B A
C B A
D C B A
*/
import java.io.*;
class Pattern2{
        public static void main(String[]arg)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter number of row");
                int row = Integer.parseInt(br.readLine());
                System.out.println("Enter number of coloumns");
                int col = Integer.parseInt(br.readLine());
                int ch=65;
		for(int i=1,j=1;i<=row&&j<=col;j++){
			if(j==i){
				System.out.println((char)ch);
				ch=ch+i;
				i++;
				j=0;
			}
			else{
				System.out.print((char)ch+" ");
				ch--;
			}
		}
	}
}


