/*Que 4: WAP to print the following pattern
Take input from the user
1  3  5  7
2  4  6  8
9  11 13 15
10 12 14 16
*/
import java.io.*;
class Pattern2{
        public static void main(String[]args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter number of rows");
                int row=Integer.parseInt(br.readLine());
                System.out.println("Enter number of coloumns");
                int col = Integer.parseInt(br.readLine());
                int num=1;
		for(int i=0,j=0;i<row&&j<col;j++){
			if(i%2==0){
				if(j==row-1){
					System.out.println(num);
					num=num-5;
					i++;
					j=-1;
				}
				else{
					System.out.print(num+" ");
					num=num+2;
				}

			}
			else{
				if(j==row-1){
					System.out.println(num);
					num++;
					j=-1;
					i++;
				}
				else{
					System.out.print(num+" ");
					num=num+2;
				}
			}
		}
	}
}


