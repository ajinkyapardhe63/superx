/*print this pattern 
  c d e f
  D C B A
  e f g h
  F E D C
  g h i j

*/
class Pattern{
        public static void main(String[]args){
                char ch = 'f';
                int typecast;

                for(int i=0,j=0;i<4&&j<=3;j++){
			if(i%2==0){
                                if(j<3){
                                        typecast=Character.toUpperCase((int)ch-2);
                                        System.out.print((char)typecast+" ");
                                        ch--;

                                }
				else if(j==3){
					typecast=Character.toUpperCase((int)ch-2);
                                        System.out.println((char)typecast+" ");
                                        ch--;
					i++;
					j=-1;
				}
			}

			if(i%2==1){
				if(j<3){
					typecast=Character.toLowerCase((int)ch+3);
                                	System.out.print((char)typecast+" ");
                                	ch++;
                                }
				else if(j==3){
					typecast=Character.toLowerCase((int)ch+3);
                                        System.out.println((char)typecast+" ");
                                        ch++;
					i++;
					j=-1;
					typecast=(int)ch+2;
                                	ch=(char)typecast;
                        	}
			}
                        
                }
        }
}

