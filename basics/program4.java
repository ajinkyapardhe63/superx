/*
 A b C d E
 e D c B
 B c D
 d C
 C
*/
class Pattern2{
	public static void main(String[]args){
		char ch='A';
		for(int i=0;i<5;i++){
			for(int j=0;j<5-i;j++){
				if(i%2==0){
					if(j%2==0)
						System.out.print(Character.toUpperCase(ch)+" ");
					else
						System.out.print(Character.toLowerCase(ch)+" ");
					ch++;
				}
				else {
					if(j%2==0)
						System.out.print(Character.toLowerCase(ch)+" ");
					else
						System.out.print(Character.toUpperCase(ch)+" ");
					ch--;
				}
			}
			if(i%2==0)
				ch--;
			else
				ch++;
			System.out.println();
		}
	}

}


					

