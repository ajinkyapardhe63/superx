/* Que 2 : WAP to print the following pattern
Take row input from user
1
2 1
3 2 1
4 3 2 1
*/
import java.io.*;
class Pattern2{
        public static void main(String[]arg)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter number of row");
                int row = Integer.parseInt(br.readLine());
                System.out.println("Enter number of coloumns");
                int col = Integer.parseInt(br.readLine());
		int num=1;
                for(int i=0,j=0;i<row&&j<col;j++){
			if(j==i){
				System.out.println(num);
				j=-1;
				i++;
				num=num+i;
			}
			else{
				System.out.print(num+" ");
				num--;
			}
		}
	}
}


