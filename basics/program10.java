import java.io.*;
class Palindrome{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a String to check if it is palindrome or not");
		String str=br.readLine();
		br.close();
		int start=0,end=str.length()-1,flag=0;
		while(start<end){
			if(str.charAt(start)!=str.charAt(end))
				flag=1;
			start++;
			end--;
		}
		if(flag==1)
			System.out.println(str+" is not a palindrome String");
		else
			System.out.println(str+" is a palindrome String");
	}
}


