/*Print this Pattern
           1
        2  3
     3  6  9
  4  8  12 16
*/  
import java.io.*;
class Pattern5{
    	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows");
		int row=Integer.parseInt(br.readLine());
		System.out.println("Enter number of columns");
		int col=Integer.parseInt(br.readLine());
	    	for (int i = 1; i <= row; i++) {
            		for (int j = 1; j<=col-i; j++) {
           			System.out.print("  ");
			}
			for (int j = 1; j<=i ; j++) {
                		System.out.print(j*i+" ");
			}
			System.out.println();
		}
	}
}
