import java.io.*;
class FindNumber{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a number");
		int num=Integer.parseInt(br.readLine());
		int max=Integer.MIN_VALUE;
		while(num!=0){
			int rem=num%10;
			if(rem>max)
				max=rem;
			num=num/10;
		}
		System.out.println("Number is "+max);
	}
}
