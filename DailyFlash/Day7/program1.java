/*Que 1: WAP to print the following pattern
Take input from the user
1 2 3 4
a b c d
5 6 7 8
e f g h
9 10 11 12
*/
import java.io.*;
class Pattern1{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Eneter number of rows");
		int row=Integer.parseInt(br.readLine());
		System.out.println("Enter number of coloumns");
		int col=Integer.parseInt(br.readLine());
		int num=1;
		char ch='a';
		for(int i=0,j=0;i<row&&j<col;j++){
			if(i%2==0){
				if(j==col-1){
					System.out.println(num++);
					j=-1;
					i++;
				}
				else
					System.out.print(num++ +" ");

			}
			else{
				if(j==col-1){
                                        System.out.println(ch++);
                                        j=-1;
                                        i++;
                                }
                                else
                                        System.out.print(ch++ +" ");
			}
		}
	}
}



