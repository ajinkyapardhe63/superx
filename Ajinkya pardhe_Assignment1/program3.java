/*Print this pattern
  5
  6 8
  7 10 13
  8 12 16 20
  9 14 19 24 29
*/
import java.io.*;
class Pattern3{
        public static void main(String[]args)throws IOException{
                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Number of rows");
                int row=Integer.parseInt(br.readLine());
                System.out.println("Enter Number of columns");
                int col=Integer.parseInt(br.readLine());
                int num=5;
                for(int i=0,j=0;i<row&&j<col;j++){
                        if(j==i){
				if(i%2==0){
				       	System.out.println(num);
           				num=num-(i*j)+(i+1)-i*2;
				}
				else{
					System.out.println(num);
					num=num-(i*j)-(i-1);
				}
                        	j=-1;
                        	i++;
                        }
                        else{
                                System.out.print(num+" ");
                                num=num+(i+1);
                        }
                }
        }
}

