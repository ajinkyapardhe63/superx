//Que 3: WAP to find the factorial of a given number.
import java.io.*;
class factorials{
        public static void main(String[]arg)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter a number ");
                int no = Integer.parseInt(br.readLine());
		int fact=1;
		for(int i=1;i<=no;i++){
			fact=fact*i;
		}
		System.out.println("Factorial of "+no+" is :"+fact);
	}
}

