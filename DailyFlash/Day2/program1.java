/*
Que 1 : WAP to print the following pattern
Take input from user
A B C D
B C D E
C D E F
D E F G
*/
import java.io.*;
class Pattern1{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
	        System.out.println("Enter no. of rows");
                int row=Integer.parseInt(br.readLine());
                System.out.println("Enter no. of coloumns");
                int col=Integer.parseInt(br.readLine());
                int num=65;
                for(int i=0,j=0;i<row&&j<col;j++){
                        if(j==row-1){
                                System.out.println((char)num);
				num=65;
                                num=num+i+1;
                                i++;
                                j=-1;
                        }
                        else{
                                System.out.print((char)num+" ");
                                num++;
			}
		}
	}
}

