//Que 3: WAP to check whether the given number is perfect or not.
import java.io.*;
class PerfectNumber{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a number to see if it is perfect number or not");
		int num=Integer.parseInt(br.readLine());
		int sum=0;
		for(int i=1;i<num;i++){
			if(num%i==0){
				sum=sum+i;
			}
		}
		if(sum==num){
			System.out.println(num+" is a perfect number");
		}
		else{
			System.out.println(num+" is not a prefect number");
		}
	}
}

