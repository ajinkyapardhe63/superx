import java.io.*;
class ToggleCase{
	public static void main(String[]args)throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a String");
		String str=br.readLine();
		br.close();
		StringBuilder sb = new StringBuilder(); 
		for(int i=0;i<str.length();i++){
			char ch=str.charAt(i);
			if(ch>='a'&&ch<='z')
				sb.append(Character.toUpperCase(ch));
			
			
			else if(ch>='A'&&ch<='Z')
				sb.append(Character.toLowerCase(ch));
			
			else
				sb.append(ch);
		}
		System.out.println(sb.toString());

	}
}

