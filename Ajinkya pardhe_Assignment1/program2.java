/*Print this pattern
  0
  3  8
  15 24 25
  48 63 80 99
*/
import java.io.*;
class Pattern2{
	public static void main(String[]args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Number of rows");
		int row=Integer.parseInt(br.readLine());
		System.out.println("Enter Number of columns");
		int col=Integer.parseInt(br.readLine());
		int count=1;
		int num=0;
		for(int i=0,j=0;i<row&&j<col;j++){
			if(j==i){
				System.out.println(num+" ");
				j=-1;
				i++;
			}
			else{
				System.out.print(num+" ");
			}
			num=num+1+(count*2);
			count++;
		}
	}
}


