// Q2) Check if the number is armstrong or not//
import java.io.*;
class Armstrong{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a Number");
		int num=Integer.parseInt(br.readLine());
		int count=0;
		int temp=num;
		while(num!=0){
			count++;
			num=num/10;
		}
		int sum=0;
		num=temp;
		while(num!=0){
			int rem=num%10;
			int mul=1;
			for(int i=0;i<count;i++){
				mul=mul*rem;
			}
			sum=sum+mul;
			num=num/10;
		}
		if(sum==temp)
			System.out.println(temp+" is an armstrong number");
		else
			System.out.println(temp+" is not an armstrong number");
	}
}
