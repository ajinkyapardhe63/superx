/*Que 5: WAP to print the occurrence of a letter in given String.
Input String: “Know the code till the core”
Alphabet : o
Output: 3
*/
import java.io.*;
class AlphabetOccurrence{
	int Occurrence(String str,char letter){
		int i=0,count=0;
		while(i<str.length()){
			if(str.charAt(i)==letter){
				count++;
			}
			i++;
		}
		return count;
	}
}
class OccurrenceMain{
	public static void main(String []args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a String");
		String str=br.readLine();
		System.out.println("Enter a letter to see its Occurrence in the String");
		char letter=(char)br.read();
		br.close();
		AlphabetOccurrence obj=new AlphabetOccurrence();
		int occ=obj.Occurrence(str,letter);
		if(occ==0)
			System.out.println("String '"+str+"' does not contains letter '"+letter+"'");
		else
			System.out.println("String '"+str+"' contains letter '"+letter+"' "+occ+" times");
	}
}


